# Modulos de Python
import socket
import sys



def proccessArguments():

    # Recibe de la linea de comandos un argumento,
    # la direccion IP del servidor o nombre de dominio,
    host_server = sys.argv[1]

    arguments = [host_server]

    return arguments
    




def constructHTTPRequest(host_server):

    # Construccion de HTTP request line
    http_method  = "GET"
    url          = "/"
    version      = "HTTP/1.1"
    request_line = http_method + " " + url + " " + version + "\r\n"
    
    
    # Construccion de HTTP header lines
    # cada parametro debe terminar con un retorno de carro
    # y un salto de linea
    host            = "Host: " + host_server
    user_agent      = "User-Agent: Mozilla/5.0 (Linux; Android 4.1.2; GT-S6810M Build/JZO54K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36"
    #user_agent      = "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/21.0"
    accept_encoding = "Accept-Encoding: identity"
    accept_language = "Accept-Language: en-US"

    header_lines    = host            + "\r\n" + \
                      user_agent      + "\r\n" + \
                      accept_encoding + "\r\n" + \
                      accept_language + "\r\n"
    
    
    # La peticion de HTTP debe terminar con un retorno de carro y 
    # un salto de linea
    blank_line = "\r\n"
    
    
    # Concatenacion de cada parametro para construir la peticion de HTTP
    HTTP_request = request_line + \
                   header_lines + \
                   blank_line


    return HTTP_request





def TCPconnection(host_server, HTTP_request):
    
    # Crea un socket de TCP
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

    
    # Conexion del cliente al servidor dado, 
    # en el puerto 80 para HTTP
    s.connect((host_server,80))
    
    
    # Envia la peticion HTTP al servidor
    s.send(HTTP_request)

    
    # Mientras reciba informacion del servidor, la guardara
    # en HTTP_response, e imprimira en pantalla
    while True:
            HTTP_response = s.recv(1024) 
            if HTTP_response == "": break  
            print HTTP_response,

    
    # Una vez que la recepcion de informacion ha terminado 
    # se cierra la conexion con el servidor
    s.close()

    
    print "\n\nConexion con el servidor finalizada\n"



arguments    = proccessArguments()
HTTP_request = constructHTTPRequest(*arguments)
TCPconnection(arguments[0],HTTP_request)


